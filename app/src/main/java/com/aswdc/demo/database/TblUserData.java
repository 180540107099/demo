package com.aswdc.demo.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class TblUserData extends MyDatabase {

    public static final String TBL_USER_DATA = "Tbl_UserData";
    public static final String USER_ID = "UserID";
    public static final String FIRST_NAME = "FirstName";
    public static final String LAST_NAME = "LastName";
    public static final String PHONE_NUMBER = "PhoneNumber";
    public static final String EMAIL = "Email";

    public TblUserData(Context context) {
        super(context);
    }

    public long insertUserDetail(String firstname, String lastname, String phoneNumber, String email) {
        long insertedId = 0;
        if (isNumberAvailable(phoneNumber)) {
            insertedId = -1;
        } else {
            SQLiteDatabase db = getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put(FIRST_NAME, firstname);
            cv.put(LAST_NAME, lastname);
            cv.put(PHONE_NUMBER, phoneNumber);
            cv.put(EMAIL, email);
            insertedId = db.insert(TBL_USER_DATA, null, cv);
            db.close();
        }
        return insertedId;
    }

    public boolean isNumberAvailable(String phoneNumber) {
        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT * FROM " + TBL_USER_DATA + " WHERE " + PHONE_NUMBER + " = ?";
        Cursor cursor = db.rawQuery(query, new String[]{phoneNumber});
        cursor.moveToFirst();
        boolean isNumberAvailable = cursor.getCount() > 0;
        cursor.close();
        db.close();
        return isNumberAvailable;
    }
}
