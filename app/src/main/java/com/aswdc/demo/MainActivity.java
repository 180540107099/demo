package com.aswdc.demo;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.aswdc.demo.database.MyDatabase;
import com.aswdc.demo.database.TblUserData;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    EditText etFirstName, etLastName, etNumber, etEmail;
    ImageView ivClose;
    TextView tvDisplay;
    Button btnSubmit;
    ImageView ivBackground;

    RadioGroup rgGender;
    RadioButton rbMale;
    RadioButton rbFemale;

    CheckBox chbCricket;
    CheckBox chbFootBall;
    CheckBox chbHockey;

    ArrayList<HashMap<String, Object>> userList = new ArrayList<>();

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("TextViewValue", tvDisplay.getText().toString());
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViewReference();
        intViewEvent();
        setTypefaceOnView();
        handleSavedInstance(savedInstanceState);


        etFirstName.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (etFirstName.getRight() - etFirstName.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        etFirstName.setText("");
                        return true;
                    }
                }
                return false;
            }
        });

        etLastName.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (etLastName.getRight() - etLastName.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        etLastName.setText("");
                        return true;
                    }
                }
                return false;
            }
        });


        etNumber.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (etNumber.getRight() - etNumber.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        etNumber.setText("");
                        return true;
                    }
                }
                return false;
            }
        });

        etEmail.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (etEmail.getRight() - etEmail.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        etEmail.setText("");
                        return true;
                    }
                }
                return false;
            }
        });

        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }

        });
    }

    void handleSavedInstance(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            tvDisplay.setText(savedInstanceState.getString("TextViewValue"));
        }
    }

    void setTypefaceOnView() {
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/BalsamiqSans-Bold.ttf");
        btnSubmit.setTypeface(typeface);
        tvDisplay.setTypeface(typeface);
        etFirstName.setTypeface(typeface);
        etLastName.setTypeface(typeface);
        etNumber.setTypeface(typeface);
        etEmail.setTypeface(typeface);
    }

    void intViewEvent() {
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isValid()) {
                    String firstname = etFirstName.getText().toString();
                    String lastname = etLastName.getText().toString();
                    String phoneNumber = etNumber.getText().toString();
                    String email = etEmail.getText().toString();
                    TblUserData tblUserData = new TblUserData(MainActivity.this);
                    long lastInsertedId = tblUserData.insertUserDetail(firstname, lastname, phoneNumber, email);
                    Toast.makeText(getApplicationContext(), lastInsertedId > 0 ?
                            "User Inserted Successfully" : "Something went wrong", Toast.LENGTH_SHORT).show();
                }
            }
        });
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /* IMPLISIT INTENT */
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.google.com"));
                startActivity(intent);
            }
        });
        tvDisplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String abc = "some one has $192 for currency conversion @70";
                int intValueGetFromEditText;
                if (etNumber.getText().toString().length() > 0) {
                    try {
                        intValueGetFromEditText = Integer.parseInt(etNumber.getText().toString());
                    } catch (Exception e) {
                        intValueGetFromEditText = 0;
                    }
                }

            }
        });
        rgGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i == R.id.rbActMale) {
                    chbCricket.setVisibility(View.VISIBLE);
                    chbFootBall.setVisibility(View.VISIBLE);
                    chbHockey.setVisibility(View.VISIBLE);
                } else if (i == R.id.rbActFemale) {
                    chbCricket.setVisibility(View.VISIBLE);
                    chbFootBall.setVisibility(View.VISIBLE);
                    chbHockey.setVisibility(View.GONE);
                }
            }
        });
    }

    void initViewReference() {
        new MyDatabase(MainActivity.this).getReadableDatabase();

        etFirstName = findViewById(R.id.etActFirstName);
        etLastName = findViewById(R.id.etActLastName);
        etNumber = findViewById(R.id.etActNumber);
        etEmail = findViewById(R.id.etActEmail);
        ivClose = findViewById(R.id.ivActClose);
        tvDisplay = findViewById(R.id.tvActDisplay);
        btnSubmit = findViewById(R.id.btnActSubmit);
        ivBackground = findViewById(R.id.ivActBackground);

        rgGender = findViewById(R.id.rgActGender);
        rbMale = findViewById(R.id.rbActMale);
        rbFemale = findViewById(R.id.rbActFemale);

        chbFootBall = findViewById(R.id.chbActFootBall);
        chbCricket = findViewById(R.id.chbActCricket);
        chbHockey = findViewById(R.id.chbActHockey);

        getSupportActionBar().setTitle(R.string.lbl_main_activity);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.dashboard_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.menuAboutUs) {
            Toast.makeText(MainActivity.this, "Menu Button Pressed", Toast.LENGTH_SHORT).show();
        } else if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    boolean isValid() {
        boolean flag = true;
        if (TextUtils.isEmpty(etFirstName.getText())) {
            etFirstName.setError(getString(R.string.lbl_enter_value_fn));
            flag = false;
        } else {
            String fName = etFirstName.getText().toString().trim();
            String fNamePattern = "[a-zA-Z]+\\.?";
            if (!(fName.matches(fNamePattern))) {
                etFirstName.setError("Enter Valid Name");
                flag = false;
            }
        }

        if (TextUtils.isEmpty(etLastName.getText())) {
            etLastName.setError(getString(R.string.lbl_enter_value_ln));
            flag = false;
        } else {
            String lName = etLastName.getText().toString().trim();
            String lNamePattern = "[a-zA-Z]+\\.?";
            if (!(lName.matches(lNamePattern))) {
                etLastName.setError("Enter Valid Name");
                flag = false;
            }
        }

        if (TextUtils.isEmpty(etNumber.getText())) {
            etNumber.setError(getString(R.string.lbl_enter_number));
            flag = false;
        } else {
            String phoneNumber = etNumber.getText().toString();
            if (phoneNumber.length() < 10) {
                etNumber.setError("Enter Valid Phone Number");
                flag = false;
            }
        }

        if (TextUtils.isEmpty(etEmail.getText())) {
            etEmail.setError(getString(R.string.lbl_enter_email_address));
            flag = false;
        } else {
            String email = etEmail.getText().toString();
            String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
            if (!email.matches(emailPattern)) {
                etEmail.setError("Enter Valid Email Address");
                flag = false;
            }
        }

        //CheckBox Validation
//        if (!(chbCricket.isChecked() && chbCricket.isChecked() && chbCricket.isChecked())) {
//            Toast.makeText(this, "Please Select any one Checkbox", Toast.LENGTH_LONG).show();
//            flag = false;
//        }

        return flag;
    }
}